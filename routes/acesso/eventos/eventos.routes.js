// visitas.js
var express = require('express');
var router = express.Router();

var mongoose = require('../../../db/mongoose');

var objSchema = {
  ativo: Boolean,
  title: String,
  start: Date,
  end: Date,
  allDay: Boolean,
  className: String
}

function loadRoute(collection, populate) {
  if (require.cache[require.resolve('../../generic/generic.js')])
    delete require.cache[require.resolve('../../generic/generic.js')];
  var genericFeatures = require('../../generic/generic.js')(collection, objSchema, populate);

  var CustomController = require('./eventos.controller')(genericFeatures.model);

  // ROTAS GENÉRICAS
  router.use('/', genericFeatures.router);
};

loadRoute('eventos', '');

module.exports = router;