
'use strict'

// EventosController.js

var fs = require('fs');
var path = require('path');
var moment = require('moment');

function EventosController(EventosModel) {
  this.model = EventosModel;
}

module.exports = function (Model) {
  return new EventosController(Model);
};