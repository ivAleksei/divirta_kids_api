'use strict'

// CriancaController.js

var fs = require('fs');
var path = require('path');
var moment = require('moment');
var md5 = require('md5');

function CriancaController(CriancaModel) {
  this.model = CriancaModel;
}

CriancaController.prototype.create = function (req, res, next) {
  let obj = req.body;
  let model = this.model;

  let create = function (obj) {

    if (obj._id && obj._id != '') {
      var err = new Error('Objeto já existente. Favor limpar formulário e refazer operação.');
      err.status = 400;
      return next(err);
    }

    if (obj._id == '')
      delete obj._id;

    obj.num_cartao = new Date().getTime();

    model.create(obj, function (err, data) {
      if (err) {
        return next(err);
      }
      res.json(data);
    });
  }

  if (Object.keys(obj).length == 0) {
    var formidable = require('formidable');
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
      var obj = JSON.parse(fields.data);
      create(obj);
    });
  } else {
    create(obj);
  }
};


CriancaController.prototype.getCriancasByResponsavel = function (req, res, next) {
  let _id = req.query.r;

  this.model.find({}, function (err, data) {
    if (err) return next(err);
    let obj = JSON.parse(JSON.stringify(data));

    if (data) {
      res.json(data.filter(function (d) {
        return d._responsaveis.find(function (r) {
          return r._id == _id;
        });
      }));
    }
  });
};

module.exports = function (Model) {
  return new CriancaController(Model);
};