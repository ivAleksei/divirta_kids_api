// criancas.js
var express = require('express');
var router = express.Router();

var mongoose = require('../../../db/mongoose');

var objSchema = {
  nome: String,
  ativo: Boolean,
  num_cartao: String,
  documento: {
    tipo: String,
    /* 0-RG, 1-CPF, 2-Passaporte */
    numero: String
  },
  _responsaveis: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'responsaveis'
  }]
};

function loadRoute(collection, populate) {
  if (require.cache[require.resolve('../../generic/generic.js')])
    delete require.cache[require.resolve('../../generic/generic.js')];
  var genericFeatures = require('../../generic/generic.js')(collection, objSchema, populate);


  var CustomController = require('./criancas.controller')(genericFeatures.model);

  router.post('/', CustomController.create.bind(CustomController));


  router.get('/resp', CustomController.getCriancasByResponsavel.bind(CustomController));

  router.use('/', genericFeatures.router);
};

loadRoute('criancas', '_responsaveis');

module.exports = router;