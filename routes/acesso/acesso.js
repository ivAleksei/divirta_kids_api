'use strict'

var express = require('express');
var router = express.Router();

router.use('/criancas', require('./criancas/criancas.routes'));
router.use('/responsaveis', require('./responsaveis/responsaveis.routes'));
router.use('/visitas', require('./visitas/visitas.routes'));
router.use('/eventos', require('./eventos/eventos.routes'));

module.exports = router;