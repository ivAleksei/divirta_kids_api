// responsaveis.js
var express = require('express');
var router = express.Router();

var mongoose = require('../../../db/mongoose');

var objSchema = {
  nome: String,
  ativo: Boolean,
  telefones: {
    principal: String,
    secundario: String
  }
};

function loadRoute(collection, populate) {
  if (require.cache[require.resolve('../../generic/generic.js')])
    delete require.cache[require.resolve('../../generic/generic.js')];
  var genericFeatures = require('../../generic/generic.js')(collection, objSchema, populate);

  router.use('/', genericFeatures.router);
};

loadRoute('responsaveis', '');

module.exports = router;