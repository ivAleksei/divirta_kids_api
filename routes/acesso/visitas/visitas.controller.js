'use strict'

// VisitasController.js

var fs = require('fs');
var path = require('path');
var moment = require('moment');
var md5 = require('md5');

function VisitasController(VisitasModel) {
  this.model = VisitasModel;
}

VisitasController.prototype.iniciarVisita = function (req, res, next) {
  let obj = req.body;
  let model = this.model;
  let errs = [];

  let inicia = function (obj, files) {
    if (obj._id && obj._id != '') {
      var err = new Error('Objeto já existente. Favor limpar formulário e refazer operação.');
      err.status = 400;
      return next(err);
    }

    if (obj._id == '')
      delete obj._id;

    if (!obj._crianca && !obj._criancas) {
      var err = new Error('Erro no início de visita. Nenhuma criança informada.');
      err.status = 400;
      return next(err);
    }

    if (obj._crianca && typeof obj._crianca != 'string') {
      var err = new Error('Erro no início de visita através de criança. Favor limpar formulário e refazer operação.');
      err.status = 400;
      return next(err);
    }


    obj.status = 0;
    if (!obj.horario)
      obj.horario = [];

    obj.horario.push({
      entrada: moment(),
      saida: null
    })

    if (obj._criancas) {
      obj.id_compartilhado = md5(moment().valueOf());
      if (typeof obj._criancas != 'object') {
        var err = new Error('Erro no início de visita através de responsável. Favor limpar formulário e refazer operação.');
        err.status = 400;
        return next(err);
      }
      if (!obj._criancas.length) {
        var err = new Error('Erro no início de visita através de responsável. Nenhuma criança informada.');
        err.status = 400;
        return next(err);
      }
    } else {
      obj._criancas = [obj._crianca]
      delete obj._crianca;
    }

    let criancasModel = model.model.db.models.criancas;
    for (let c in obj._criancas) {

      criancasModel.findOne({
        _id: obj._criancas[c]
      }, function (err, data) {
        if (err) return next(err);

        if (!data) {
          var err = new Error('Erro no início de visita através de criança. Criança informada não encontrada.');
          err.status = 400;
          errs.push(err);
        }
      })
    }

    form.uploadDir = path.join(__dirname, '../../../files/visitas/');

    fs.stat(form.uploadDir, function (err, stats) {
      if (err)
        if (err.code == 'ENOENT') {
          fs.mkdirSync(form.uploadDir);
        }

      for (var f in files) {
        var file = files[f];

        obj.imagem = file.name;

        fs.rename(file.path, path.join(form.uploadDir, file.name));
      }
    })


    let retorno = [];

    model.find({
      ativo: true,
      status: {
        $in: [0, 1]
      }
    }, function (err, data) {
      let sapateiras = data.map(function (d) {
        return d.sapateira
      })

      let newSapateira = 0;
      for (let c in obj._criancas) {
        model.find({
          _crianca: obj._criancas[c],
          status: {
            $in: [0, 1, 2]
          }
        }, function (err, data) {
          if (err) return next(err);
          if (data.length) {
            var err = new Error('Erro no início de visita. Criança já se encontra cadastrada em uma visita.');
            err.status = 400;

            return next(err);
          } else {
            for (let i = 0; i < 20; i++) {
              if (sapateiras.indexOf(i) === -1) {
                let registro = {
                  ativo: obj.ativo,
                  status: obj.status,
                  observacoes: obj.observacoes,
                  sapateira: i,
                  horario: obj.horario,
                  imagem: obj.imagem,
                  id_compartilhado: obj.id_compartilhado,
                  _crianca: obj._criancas[c],
                  _responsavel: obj._responsavel,
                  _pagamento: null
                };

                sapateiras.push(i);
                model.create(registro, function (err, data) {
                  if (err) return next(err);

                  retorno.push(data);
                  if (c == obj._criancas.length - 1) {
                    if (errs.length)
                      return res.json(errs);

                    res.json(retorno);
                  }
                });

                break;
              } else {
                if (i == 19) {
                  var err = new Error('Erro no início de visita através de responsável. Todas as sapateiras estão lotadas.');
                  err.status = 400;
                  errs.push(err);
                }
              }
            }
          }
        })

      }
    })
  }

  if (Object.keys(obj).length == 0) {
    var formidable = require('formidable');
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
      var obj = JSON.parse(fields.data);
      inicia(obj, files);
    });
  } else {
    inicia(obj, null);
  }
};

VisitasController.prototype.pausarVisita = function (req, res, next) {
  let model = this.model;
  let obj = req.body;

  let persist = function (obj, files) {

    let _id = obj._id;
    if (!_id || _id == '') {
      let err = new Error('Não foi possível pausar visita. Identificador não informado.');
      err.status = 400;
      return next(err);
    }

    if (obj.status != 1) {
      let err = new Error('Não foi possível pausar visita. Status informado inválido.');
      err.status = 400;
      return next(err);
    }
    model.findOne(_id, function (err, data) {
      if (!data.length) {
        let err = new Error('Não foi possível pausar visita. Identificador inválido.');
        err.status = 400;
        return next(err);
      }
      let obj = data[0];

      if (obj.status == 1) {
        let err = new Error('Não foi possível pausar visita. Visita já se encontra pausada.');
        err.status = 400;
        return next(err);
      }

      if (obj.status == 2) {
        let err = new Error('Não foi possível pausar visita. Visita já encerrada.');
        err.status = 400;
        return next(err);
      }

      if (obj.status == 3) {
        let err = new Error('Não foi possível pausar visita. Visita já possui um registro de pagamento vinculado.');
        err.status = 400;
        return next(err);
      }

      obj.horario[obj.horario.length - 1].saida = moment();
      obj.status = 1;

      model.update(_id, obj, function (err, data) {
        if (err) return next(err);
        model.findOne(_id, function (err, data) {
          if (err) return next(err);
          let retorno = data[0];

          setTimeout(function () {
            model.findOne(_id, function (err, visita) {
              if (err) return next(err);

              if (visita[0].status == 1) {
                obj.horario[obj.horario.length - 1].saida = null;
                obj.status = 0;

                model.update(_id, obj, function (err, data) {
                  if (err) return next(err);
                });
              }
            });
          }, 10000)
          res.json(retorno);
        });
      });
    });
  }

  if (Object.keys(obj).length == 0) {
    var formidable = require('formidable');
    var form = new formidable.IncomingForm();

    form.parse(req, function (err, fields, files) {
      var obj = JSON.parse(fields.data);
      persist(obj, files);
    });
  } else {
    persist(obj);
  }
};

VisitasController.prototype.continuarVisita = function (req, res, next) {
  let model = this.model;
  let obj = req.body;

  let persist = function (obj, files) {

    let _id = obj._id;
    if (!_id || _id == '') {
      let err = new Error('Não foi possível continuar visita. Identificador não informado.');
      err.status = 400;
      return next(err);
    }

    if (obj.status != 0) {
      let err = new Error('Não foi possível continuar visita. Status informado inválido.');
      err.status = 400;
      return next(err);
    }


    model.findOne(_id, function (err, data) {
      if (!data.length) {
        let err = new Error('Não foi possível continuar visita. Identificador inválido.');
        err.status = 400;
        return next(err);
      }
      let obj = data[0];

      if (obj.status == 0) {
        let err = new Error('Não foi possível continuar visita. Visita não se encontra pausada.');
        err.status = 400;
        return next(err);
      }

      if (obj.status == 2) {
        let err = new Error('Não foi possível continuar visita. Visita já encerrada.');
        err.status = 400;
        return next(err);
      }

      if (obj.status == 3) {
        let err = new Error('Não foi possível continuar visita. Visita já possui um registro de pagamento vinculado.');
        err.status = 400;
        return next(err);
      }

      if (obj.horario[obj.horario.length - 1].saida) {
        obj.horario.push({
          entrada: moment(),
          saida: null
        })
      }

      obj.status = 0;

      model.update(_id, obj, function (err, data) {
        if (err) return next(err);
        model.findOne(_id, function (err, data) {
          if (err) return next(err);
          let retorno = data[0];
          res.json(retorno);
        });
      });
    });
  }

  if (Object.keys(obj).length == 0) {
    var formidable = require('formidable');
    var form = new formidable.IncomingForm();

    form.parse(req, function (err, fields, files) {
      var obj = JSON.parse(fields.data);
      persist(obj, files);
    });
  } else {
    persist(obj);
  }
};

VisitasController.prototype.encerrarVisita = function (req, res, next) {
  let model = this.model;

  let obj = req.body;

  let persist = function (obj, files) {

    let _id = obj._id;
    let close_all = obj.close_all;
    let time_out = moment();

    if (!_id || _id == '') {
      let err = new Error('Não foi possível encerrar a visita. Identificador não informado.');
      err.status = 400;
      return next(err);
    }

    if (obj.status != 2) {
      let err = new Error('Não foi possível encerrar a visita. Status informado inválido.');
      err.status = 400;
      return next(err);
    }

    model.findOne(_id, function (err, data) {
      if (err || !data.length) {
        let err = new Error('Não foi possível encerrar a visita. Identificador inválido.');
        err.status = 400;
        return next(err);
      }

      let obj = data[0];

      if (obj.status == 2) {
        let err = new Error('Não foi possível encerrar a visita. Visita já encerrada.');
        err.status = 400;
        return next(err);
      }

      let close = function (obj, saida, retorno) {
        let _id = obj._id;
        obj.status = 2;
        obj.horario[obj.horario.length - 1].saida = time_out;

        model.update(_id, obj, function (err, data) {
          if (err) return next(err);

          model.findOne(_id, function (err, data) {
            if (err) return next(err);
            retorno.push(data[0])
            if (saida) {
              if (obj.id_compartilhado) {
                res.json(retorno);
              } else {
                res.json(data[0]);
              }
            }
          });
        })
      }

      if (obj.id_compartilhado) {
        if (typeof close_all == 'undefined') {
          let err = new Error('Não foi possível encerrar a visita. Parâmetro de fechamento compartilhado não especificado.');
          err.status = 400;
          return next(err);
        } else {
          if (close_all) {
            model.find({
              id_compartilhado: obj.id_compartilhado
            }, function (err, data) {
              let retorno = [];
              for (let d in data) {
                close(data[d], (d == data.length - 1), retorno);
              }
            })
          } else {
            close(obj, true, []);
          }
        }
      } else {
        close(obj, true, []);
      }

    });
  }

  if (Object.keys(obj).length == 0) {
    var formidable = require('formidable');
    var form = new formidable.IncomingForm();

    form.parse(req, function (err, fields, files) {
      var obj = JSON.parse(fields.data);
      persist(obj, files);
    });
  } else {
    persist(obj);
  }
};

VisitasController.prototype.gerarPagamento = function (req, res, next) {
  let model = this.model;
  let obj = req.body;

  let pagamentosModel = model.model.db.models.pagamentos;

  let persist = function (obj, files) {

    let _id = obj._id;
    let close_all = obj.close_all
    let one_pay = null || obj.one_pay

    if (!_id || _id == '') {
      let err = new Error('Não foi possível gerar linha de cobrança. Identificador não informado.');
      err.status = 400;
      return next(err);
    }

    if (obj.status != 3) {
      let err = new Error('Não foi possível gerar linha de cobrança. Status informado inválido.');
      err.status = 400;
      return next(err);
    }

    model.findOne(_id, function (err, data) {
      if (!data.length) {
        let err = new Error('Não foi possível gerar linha de cobrança. Identificador inválido.');
        err.status = 400;
        return next(err);
      }
      let obj = data[0];

      if (obj.status == 3) {
        let err = new Error('Não foi possível gerar linha de cobrança. Cobrança lançada aguardando pagamento.');
        err.status = 400;
        return next(err);
      }
      if (!obj.horario) {
        let err = new Error('Não foi possível gerar linha de cobrança. Horário de entrada e saída não especificado.');
        err.status = 400;
        return next(err);
      } else {
        if (obj.horario.some((h => h.entrada == null))) {
          let err = new Error('Não foi possível gerar linha de cobrança. Horário de entrada não especificado.');
          err.status = 400;
          return next(err);
        }


        if (obj.horario.some((h => h.saida == null))) {
          let err = new Error('Não foi possível gerar linha de cobrança. Horário de saída não especificado.');
          err.status = 400;
          return next(err);
        }
      }

      var newPayment = {};

      let calculaPermanencia = function (table_horario) {
        // entrada => array de horarios
        // saida => permanencia em segundos
        let permanencia = 0;
        for (let h of table_horario) {
          let permanenciaObj = moment.duration(moment(h.saida).diff(moment(h.entrada)));
          permanencia += permanenciaObj.asSeconds();
        }
        return permanencia;
      }

      let calculaValorPermanencia = function (permanencia) {
        // entrada => permanencia em segundos
        // saida => valor em minutos
        return permanencia ? Math.ceil(permanencia / 60) : 0;
      }

      let generate = function (obj, saida, retorno, valor_onePay) {
        let _id = obj._id;
        console.log('aqui')
        console.log(obj)
        console.log(valor_onePay)
        if (valor_onePay) {

          newPayment = {
            ativo: true,
            status: false,
            _forma_pagamento: null,
            valor: valor_onePay,
            desconto: 0,
            data_movimento: obj[0].horario.saida,
            descricao: 'Pagamento de Visita',
            one_pay: true
          }

          pagamentosModel.create(newPayment, function (err, data) {
            if (err) {
              return next(err);
            }

            for (let k in obj) {
              model.update(obj[k]._id, {
                _pagamento: data._id,
                status: 3
              }, function (err, data) {
                if (err) return next(err);
              });
            }
            res.json(data);
          });

        } else {

          newPayment = {
            ativo: true,
            status: false,
            _forma_pagamento: null,
            valor: calculaValorPermanencia(calculaPermanencia(obj.horario)),
            desconto: 0,
            data_movimento: obj.horario.saida,
            descricao: 'Pagamento de Visita',
            one_pay: false
          }

          console.log(newPayment);

          pagamentosModel.create(newPayment, function (err, data) {
            if (err) {
              return next(err);
            }
            model.update(obj._id, {
              _pagamento: data._id,
              status: 3
            }, function (err, data) {
              if (err) return next(err);
              retorno.push(data[0])
              if (saida) {
                if (obj.id_compartilhado) {
                  res.json(retorno);
                } else {
                  res.json(data[0]);
                }
              }
            });
          });
        }

      }

      if (obj.id_compartilhado) {
        if (typeof close_all == 'undefined') {
          let err = new Error('Não foi possível encerrar a visita. Parâmetro de fechamento compartilhado não especificado.');
          err.status = 400;
          return next(err);
        } else {
          if (close_all) {
            model.find({
              id_compartilhado: obj.id_compartilhado
            }, function (err, data) {
              if (one_pay) {
                let estadia = data.map(d => {
                  return calculaValorPermanencia(calculaPermanencia(d.horario));
                });

                let valor = estadia.reduce(function (valorAnterior, valorAtual, indice, array) {
                  return valorAnterior + valorAtual;
                })
                let retorno = [];

                generate(data, true, retorno, valor);

              } else {
                let retorno = [];
                for (let d in data) {
                  generate(data[d], (d == data.length - 1), retorno);
                }
              }
            })
          } else {
            generate(obj, true, []);
          }
        }
      } else {
        generate(obj, true, []);
      }
    });
  }

  if (Object.keys(obj).length == 0) {
    var formidable = require('formidable');
    var form = new formidable.IncomingForm();

    form.parse(req, function (err, fields, files) {
      var obj = JSON.parse(fields.data);
      persist(obj, files);
    });
  } else {
    persist(obj);
  }
};

VisitasController.prototype.getPermanencia = function (req, res, next) {
  let _id = req.params._id;


  this.model.findOne({
    _id: _id
  }, function (err, data) {
    res.json({
      _id: _id,
      entrada: data[0].horario.entrada
    });
  })
};

VisitasController.prototype.getAbertas = function (req, res, next) {
  this.model.find({
    status: {
      $in: [0, 1, 3]
    }
  }, function (err, data) {
    res.json(data);
  })
};

VisitasController.prototype.getPhoto = function (req, res, next) {
  var _id = req.params._id;
  this.model.find({
    _id: _id
  }, function (err, data) {
    if (data[0] && data[0].imagem) {
      var img = fs.readFileSync(path.join(__dirname, '../../../files/visitas/' + data[0].imagem));
      res.writeHead(200, {
        'Content-Type': 'image/png'
      });
      res.end(img, 'binary');
    } else {
      var img = fs.readFileSync(path.join(__dirname, '../../../public/img/default-avatar.png'));
      res.writeHead(200, {
        'Content-Type': 'image/png'
      });
      res.end(img, 'binary');
    }
  })
};

VisitasController.prototype.getInfoPagamento = function (req, res, next) {
  let _id = req.params._id;
  let model = this.model;

  model.find({
    _id: _id
  }, function (err, data) {

    if (data[0].id_compartilhado) {
      if (!data[0]._pagamento) {
        let err = new Error('Não há pagamento relacionado a esta visita.');
        err.status = 400;
        return next(err);
      } else {
        if (data[0]._pagamento.one_pay) {
          model.find({
            id_compartilhado: data[0].id_compartilhado
          }, function (err, data) {
            res.json({
              id_compartilhado: data[0].id_compartilhado,
              _id: data.map(d => {
                return d._id
              }),
              status: data[0].status,
              observacoes: data[0].observacoes,
              sapateira: data.map(d => {
                return d.sapateira
              }),
              imagem: data[0].imagem,
              _criancas: data.map(d => {
                let obj = {
                  _id: d._crianca._id,
                  nome: d._crianca.nome,
                  ativo: d.ativo,
                  num_cartao: d._crianca.num_cartao,
                  _responsaveis: d._crianca._responsaveis,
                  documento: d._crianca.documento,
                  horario: d.horario
                }
                return obj;
              }),
              _responsavel: data[0]._responsavel,
              _pagamento: data[0]._pagamento
            });
          });
        } else {
          model.find({
            id_compartilhado: data[0].id_compartilhado
          }, function (err, data) {
            res.json({
              id_compartilhado: data[0].id_compartilhado,
              _id: data.map(d => {
                return d._id
              }),
              status: data[0].status,
              observacoes: data[0].observacoes,
              sapateira: data.map(d => {
                return d.sapateira
              }),
              imagem: data[0].imagem,
              _criancas: data.map(d => {
                return d._crianca;
              }),
              _responsavel: data[0]._responsavel,
              _pagamento: data.map(p => {
                return p
              })
            });
          });
        }
      }

    }else{
      res.json(data[0])
    }
  })
};

module.exports = function (Model) {
  return new VisitasController(Model);
};