// visitas.js
var express = require('express');
var router = express.Router();

var mongoose = require('../../../db/mongoose');

/*[   0        1        2            3            4    ] */
/*[aberta, pausada, encerrada, em pagamento, finalizada],*/

var objSchema = {
  ativo: Boolean,
  status: Number,
  observacoes: String,
  sapateira: Number,
  horario: [{
    entrada: Date,
    saida: Date,
  }],
  id_compartilhado: String,
  _crianca: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'criancas'
  },
  _responsavel: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'responsaveis'
  },
  _pagamento: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'pagamentos'
  },
  imagem: String
};

function loadRoute(collection, populate) {
  if (require.cache[require.resolve('../../generic/generic.js')])
    delete require.cache[require.resolve('../../generic/generic.js')];
  var genericFeatures = require('../../generic/generic.js')(collection, objSchema, populate);

  var CustomController = require('./visitas.controller')(genericFeatures.model);

  router.get('/time/:_id', CustomController.getPermanencia.bind(CustomController));

  router.get('/abertas', CustomController.getAbertas.bind(CustomController));

  router.get('/img/:_id', CustomController.getPhoto.bind(CustomController));

  router.get('/info_payment/:_id', CustomController.getInfoPagamento.bind(CustomController));
  
  router.post('/start', CustomController.iniciarVisita.bind(CustomController));
  
  router.put('/restart', CustomController.continuarVisita.bind(CustomController));

  router.put('/pause', CustomController.pausarVisita.bind(CustomController));

  router.post('/gen_pay', CustomController.gerarPagamento.bind(CustomController));

  router.put('/end', CustomController.encerrarVisita.bind(CustomController));
  
  // ROTAS GENÉRICAS
  router.use('/', genericFeatures.router);
};

loadRoute('visitas', '_crianca _criancas _responsavel _pagamento _pagamento._forma_pagamento');

module.exports = router;