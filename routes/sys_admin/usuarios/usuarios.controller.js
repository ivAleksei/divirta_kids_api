'use strict'

// UsuarioController.js

var md5 = require('md5');
var fs = require('fs');
var path = require('path');
var moment = require('moment');
var jwt = require('jwt-simple');
var config = require('config');

function UsuarioController(UsuarioModel) {
  this.model = UsuarioModel;
}

UsuarioController.prototype.info = function (req, res, next) {
  let _id = req.params._id;
  console.log('info');
  this.model.findOne({
    _id: _id
  }, function (err, data) {
    if (err) return next(err);
    let user = data[0];
    res.json({
      _id: user._id,
      pessoa: user.pessoa,
      modulosAutorizados: user._modulos.map(function (m) {
        return {
          _id: m._id,
          nome: m.nome,
          codigo: m.codigo,
          icone: m.icone,
          paginas: user._paginas.filter(function (p) {
            return p._modulo.toString() == m._id.toString();
          }).map(function (p) {
            return {
              _id: p._id,
              nome: p.nome,
              codigo: p.codigo
            }
          })
        };
      })
    })
  });
  // this.model.find({}, function (err, user) {
  //   if (err) return next(err);

  // })
};

UsuarioController.prototype.logIn = function (req, res, next) {
  let obj = req.body;
  let model = this.model;

  let logar = function (obj) {

    let encrypt = {
      login: md5(obj.login)
    }
    encrypt.password = md5(encrypt.login + obj.password);

    model.find({
      login: encrypt.login,
      password: encrypt.password
    }, function (err, data) {
      if (err) return next(err);

      if (data.length) {
        let user = data[0];
        var expires = moment().add(1, 'hour').valueOf();
        var token = jwt.encode({
          user: user.login,
          exp: expires
        }, config.get('jwtTokenSecret'));
        res.json({
          _id: user._id,
          login: user.login,
          pessoa: user.pessoa,
          modulosAutorizados: user._modulos.map(function (m) {
            return {
              _id: m._id,
              nome: m.nome,
              codigo: m.codigo,
              icone: m.icone,
              paginas: user._paginas.filter(function (p) {
                return p._modulo.toString() == m._id.toString();
              }).map(function (p) {
                return {
                  _id: p._id,
                  nome: p.nome,
                  codigo: p.codigo
                }
              })
            };
          }),
          token: token
        });
      } else {
        model.find({
          login: obj.login
        }, function (err, data) {
          if (err) return next(err);
          if (data.length) {
            var err = new Error('Senha incorreta. Tente novamente.');
            err.status = 401;
            next(err);
          } else {
            var err = new Error('O sistema não reconhece este usuário.');
            err.status = 401;
            next(err);
          }
        })
      }
    });
  }

  if (Object.keys(obj).length == 0) {
    var formidable = require('formidable');
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
      var obj = JSON.parse(fields.data);
      logar(obj);
    });
  } else {
    logar(obj);
  }
};

UsuarioController.prototype.md5 = function (req, res, next) {
  let obj = req.body;
  let model = this.model;

  let getEncrypts = function (obj) {
    let loginHashFront = md5(obj.login);
    let maskFront = {
      login: loginHashFront,
      password: md5(loginHashFront + obj.password)
    }

    let loginHashBack = md5(loginHashFront);
    let maskBack = {
      login: loginHashBack,
      password: md5(loginHashBack + maskFront.password)
    }

    res.json({
      clientForm: obj,
      hashFront: maskFront,
      hashBack: maskBack,
    })
  }

  if (Object.keys(obj).length == 0) {
    var formidable = require('formidable');
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
      var obj = JSON.parse(fields.data);
      getEncrypts(obj);
    });
  } else {
    getEncrypts(obj);
  }
};

UsuarioController.prototype.create = function (req, res, next) {
  let obj = req.body;
  let model = this.model;

  let create = function (obj) {

    obj.login = md5(obj.login);
    obj.password = md5(obj.login + obj.password);

    if (obj._id && obj._id != '') {
      var err = new Error('Objeto já existente. Favor limpar formulário e refazer operação.');
      err.status = 400;
      return next(err);
    }

    if (obj._id == '')
      delete obj._id;

    model.create(obj, function (err, data) {
      if (err) {
        return next(err);
      }
      res.json(data);
    });
  }

  if (Object.keys(obj).length == 0) {
    var formidable = require('formidable');
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
      var obj = JSON.parse(fields.data);
      create(obj);
    });
  } else {
    create(obj);
  }
};

UsuarioController.prototype.autorize = function (req, res, next) {
  let obj = req.body;

  let autorize = function (obj) {
    res.json(obj);
  }

  if (Object.keys(obj).length == 0) {
    var formidable = require('formidable');
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
      var obj = JSON.parse(fields.data);
      autorize(obj);
    });
  } else {
    autorize(obj);
  }
}

module.exports = function (Model) {
  return new UsuarioController(Model);
};