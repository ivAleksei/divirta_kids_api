// Usuarios.js
var express = require('express');
var router = express.Router();
var mongoose = require('../../../db/mongoose');

var objSchema = {
  login: String,
  password: String,
  ativo: Boolean,
  pessoa: {
    nome: String
  },
  _modulos: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'modulos'
  }],
  _paginas: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'paginas'
  }]
};

function loadRoute(collection, populate) {
  if (require.cache[require.resolve('../../generic/generic.js')])
    delete require.cache[require.resolve('../../generic/generic.js')];
  var genericFeatures = require('../../generic/generic.js')(collection, objSchema, populate);

  var CustomController = require('./usuarios.controller')(genericFeatures.model);

  router.get('/info/:_id', CustomController.info.bind(CustomController));

  router.post('/login', CustomController.logIn.bind(CustomController));

  router.post('/md5', CustomController.md5.bind(CustomController));

  router.post('/autorize', CustomController.autorize.bind(CustomController));
  
  router.post('/', CustomController.create.bind(CustomController));

  // ROTAS GENÉRICAS
  router.use('/', genericFeatures.router);
};

loadRoute('usuarios', '_modulos _paginas');

module.exports = router;