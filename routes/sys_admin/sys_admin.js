'use strict'

var express = require('express');
var router = express.Router();

router.use('/modulos', require('./modulos/modulos.routes'));
router.use('/paginas', require('./paginas/paginas.routes'));
router.use('/usuarios', require('./usuarios/usuarios.routes'));

router.use('/parentescos', require('./parentescos/parentescos.routes'));


module.exports = router;