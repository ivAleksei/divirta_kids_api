// modulos.js
var express = require('express');
var router = express.Router();

var mongoose = require('../../../db/mongoose');

var objSchema = {
  descricao: String,
  ativo: Boolean
};
  
function loadRoute(collection, populate) {
  if (require.cache[require.resolve('../../generic/generic.js')])
    delete require.cache[require.resolve('../../generic/generic.js')];
  var genericFeatures = require('../../generic/generic.js')(collection, objSchema, populate);

  
  router.use('/', genericFeatures.router);
};
  
loadRoute('parentescos', '');

module.exports = router;