// paginas.js
var express = require('express');
var router = express.Router();

var mongoose = require('../../../db/mongoose');

var objSchema = {
  nome: String,
  codigo: String,
  ativo: Boolean,
  _modulo: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'modulos'
  },
};


function loadRoute(collection, populate) {
  if (require.cache[require.resolve('../../generic/generic.js')])
    delete require.cache[require.resolve('../../generic/generic.js')];
  var genericFeatures = require('../../generic/generic.js')(collection, objSchema, populate);

  router.use('/', genericFeatures.router);
};

loadRoute('paginas', '_modulo');

module.exports = router;