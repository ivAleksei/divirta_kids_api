'use strict'

var express = require('express');
var router = express.Router();

router.use('/formas_pagamentos', require('./formas_pagamentos/formas_pagamentos.routes'));
router.use('/pagamentos', require('./pagamentos/pagamentos.routes'));

module.exports = router;