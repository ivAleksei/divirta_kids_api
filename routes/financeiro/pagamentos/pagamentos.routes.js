// pagamentos.js
var express = require('express');
var router = express.Router();

var mongoose = require('../../../db/mongoose');

var objSchema = {
  ativo: Boolean,
  descricao: String,
  status: Boolean,
  _forma_pagamento: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'formas_pagamentos'
  },
  valor: Number,
  desconto: {
    tipo: String,
    valor: Number
  },
  data_movimento: Date,
  one_pay: Boolean
};

function loadRoute(collection, populate) {
  if (require.cache[require.resolve('../../generic/generic.js')])
    delete require.cache[require.resolve('../../generic/generic.js')];
  var genericFeatures = require('../../generic/generic.js')(collection, objSchema, populate);

  var CustomController = require('./pagamentos.controller')(genericFeatures.model);
  // console.log(genericFeatures.model);
  router.put('/pay', CustomController.finalizaPagamento.bind(CustomController));
  router.get('/mov', CustomController.getMovimentoMes.bind(CustomController));
  router.get('/bal', CustomController.getBalancoMensal.bind(CustomController));

  router.get('/anos', CustomController.getAnosMovimentos.bind(CustomController));


  // ROTAS GENÉRICAS
  router.use('/', genericFeatures.router);
};

loadRoute('pagamentos', '_forma_pagamento');

module.exports = router;