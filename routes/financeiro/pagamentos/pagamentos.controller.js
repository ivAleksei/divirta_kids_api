'use strict'

// PagamentosController.js

var fs = require('fs');
var path = require('path');
var moment = require('moment');

function PagamentosController(PagamentosModel) {
  this.model = PagamentosModel;
}

PagamentosController.prototype.getMovimentoMes = function (req, res, next) {
  let filtro = req.query;

  let query = {
    ativo: true,
    data_movimento: {
      $gte: new Date(moment(filtro.ano + '-' + filtro.mes, 'YYYY-MM').format()),
      $lt: new Date(moment(filtro.ano + '-' + filtro.mes, 'YYYY-MM').add(1, 'month').format())
    }
  }

  this.model.find(query, function (err, data) {
    if (err) return next(err);
    res.json(data);
  });
};

PagamentosController.prototype.getBalancoMensal = function (req, res, next) {
  let filtro = req.query;

  let query = [{
      $match: {
        ativo: true,
        data_movimento: {
          $gte: new Date(moment(filtro.ano + '-' + filtro.mes, 'YYYY-MM').format()),
          $lt: new Date(moment(filtro.ano + '-' + filtro.mes, 'YYYY-MM').add(1, 'month').format())
        }
      }
    },
    {
      $group: {
        _id: {
          'status': '$status'
        },
        'valor': {
          $sum: '$valor'
        }
      }
    }
  ];

  this.model.aggregate(query, function (err, data) {
    if (err) return next(err);
    res.json([{
      _id: {
        'status': false
      },
      'valor': data.filter(function (d) {
        return d._id.status == false
      }).map(function (d) {
        return d.valor
      })[0] || 0
    }, {
      _id: {
        'status': true
      },
      'valor': data.filter(function (d) {
        return d._id.status == true
      }).map(function (d) {
        return d.valor
      })[0] || 0
    }]);
  });
};

PagamentosController.prototype.getAnosMovimentos = function (req, res, next) {
  function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }
  this.model.find({}, function (err, data) {
    if (data.length) {
      res.json(data.map(function (r) {
          return new Date(r.data_movimento).getFullYear();
        }).filter(onlyUnique)
        .map(function (r) {
          return {
            ano: r
          }
        })
      )
    } else {
      var anos = {
        ano: new Date().getFullYear()
      }
      res.json([anos])
    }

  })
};

PagamentosController.prototype.finalizaPagamento = function (req, res, next) {
  let obj = req.body;
  let model = this.model;

  let finaliza = function (obj) {
    let _id = obj._id;

    if (!obj.status || !obj._id_visita || !obj._id) {
      var err = new Error('Não foi possível efetuar o pagamento. Favor limpar formulário e refazer operação.');
      err.status = 400;
      return next(err);
    }


    let formas_pagamentosModel = model.model.db.models.formas_pagamentos;
    let visitasModel = model.model.db.models.visitas;

    formas_pagamentosModel.find({
      _id: obj._forma_pagamento
    }, function (err, data) {
      if (err) {
        return next(err);
      }
      if (!data.length) {
        var err = new Error('Forma de pagamento não encontrada. Favor limpar formulário e refazer operação.');
        err.status = 400;
        return next(err);
      } else {
        // FIND PAGAMENTO PARA VALIDAR DADOS
        model.findOne({
          _id: obj._id
        }, function (err, data) {
          if (err) {
            return next(err);
          }
          if (!data.length) {
            var err = new Error('Pagamento não encontrado. Favor verificar encerramento de visita.');
            err.status = 400;
            return next(err);
          }
          if (data[0].valor < obj.desconto) {
            var err = new Error('Pagamento não efetuado. Não é possível um desconto maior que o valor calculado.');
            err.status = 400;
            return next(err);
          }

          // MONTA PAGAMENTO
          let newPayment = data[0];
          newPayment._forma_pagamento = obj._forma_pagamento;
          newPayment.desconto = obj.desconto;
          newPayment.data_movimento = moment();
          newPayment.status = true;

          if (obj.id_compartilhado && data[0].one_pay) {
            visitasModel.update({
              id_compartilhado: obj.id_compartilhado
            }, {
              status: 4
            }, {
              multi: true
            }, function (err, data) {
              if (err) return next(err);
            });

            model.update(_id, newPayment, function (err, data) {
              if (err) return next(err);
              model.findOne({
                _id: _id
              }, function (err, data) {
                if (err) return next(err);
                res.json(data);
              });
            });
          } else {
            visitasModel.update({
              _id: obj._id_visita
            }, {
              status: 4
            }, function (err, data) {
              if (err) return next(err);
            });

            model.update(_id, newPayment, function (err, data) {
              if (err) return next(err);
              model.findOne({
                _id: _id
              }, function (err, data) {
                if (err) return next(err);
                res.json(data);
              });
            });
          }




          //       let _id_visita = obj._id_visita;

          //       visitasModel.update({
          //         _id: _id_visita
          //       }, {
          //         status: 4
          //       }, function (err, data) {
          //         if (err) return next(err);

          //       });

          //       

        });
      }
    })
  }

  if (Object.keys(obj).length == 0) {
    var formidable = require('formidable');
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
      var obj = JSON.parse(fields.data);
      finaliza(obj);
    });
  } else {
    finaliza(obj);
  }
};

module.exports = function (Model) {
  return new PagamentosController(Model);
};