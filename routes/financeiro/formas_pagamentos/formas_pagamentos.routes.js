// formas_pagamentos.js
var express = require('express');
var router = express.Router();

var mongoose = require('../../../db/mongoose');

var objSchema = {
  ativo: Boolean,
  descricao: String,
  parcelamento: {
    tipo: Number, /*[Simples, Parcelamento]*/
    numParcelas: Number,
    intervalo: String /*Default: M */
  }
};

function loadRoute(collection, populate) {
  if (require.cache[require.resolve('../../generic/generic.js')])
    delete require.cache[require.resolve('../../generic/generic.js')];
  var genericFeatures = require('../../generic/generic.js')(collection, objSchema, populate);

  // ROTAS GENÉRICAS
  router.use('/', genericFeatures.router);
};

loadRoute('formas_pagamentos', '');

module.exports = router;