var express = require('express');
var router = express.Router();
var qrcode = require('qrcode');


router.get('/', function (req, res) {
  res.redirect('/api')
})

router.get('/401', function (req, res) {
  res.render('401')
});

router.get('/403', function (req, res) {
  res.render('403')
});

router.get('/404', function (req, res) {
  res.render('404')
});

router.use('/api', require('./api'));

router.use('/info', require('./info'));

module.exports = router;