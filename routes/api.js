'use strict'

var express = require('express');
var router = express.Router();

var fs = require('fs');
var path = require('path');
var pkgAPI = require('../package.json');
var qrcode = require('qrcode');

router.get('/', function (req, res) {
  res.render('homeAPI', {
    info: pkgAPI
  })
});

router.get('/qr', function (req, res, next) {
  let code = String(new Date().getTime()); //req.query.q;

  qrcode.toDataURL(code, function (err, url) {
    var img = Buffer.from(url.replace('data:image/png;base64,', ''), 'base64');
    res.writeHead(200, {
      'Content-Type': 'image/png'
    });
    res.end(img, 'binary');
  });
})

router.use('/sys_admin', require('./sys_admin/sys_admin'));
router.use('/acesso', require('./acesso/acesso'));
router.use('/financeiro', require('./financeiro/financeiro'));


module.exports = router;