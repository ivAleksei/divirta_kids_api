var express = require('express');
var fs = require('fs');
var path = require('path');
var router = express();

var mongoose = require('../../db/mongoose');

var auth = require('../auth');

var routerCreater = function (collection, objSchema, populate) {

  let GenericModel = require('./generic.model')(mongoose, collection, objSchema, populate);
  let GenericController = require('./generic.controller')(GenericModel);


  // GET ATIVOS
  router.get('/', auth, GenericController.getActive.bind(GenericController));

  // GET TODOS (INCLUSIVE NÃO ATIVOS)
  router.get('/all', auth, GenericController.getAll.bind(GenericController));

  // GET TODOS (INCLUSIVE NÃO ATIVOS)
  router.get('/q', auth, GenericController.getByQuery.bind(GenericController));

  // GET UM PELO ID
  router.get('/:_id', auth, GenericController.getById.bind(GenericController));

  // POST - CRIAÇÃO DE OBJETO
  router.post('/', auth, GenericController.create.bind(GenericController));

  // POST - CRIAÇÃO DE ARRAY
  router.post('/arr', auth, GenericController.createArray.bind(GenericController));

  // PUT - ATUALIZAÇÃO DE OBJETO POR UM ID
  router.put('/:_id', auth, GenericController.update.bind(GenericController));

  // DELETE - EXLUIR moduloS POR QUERY
  router.delete('/q', auth, GenericController.removeQuery.bind(GenericController));

  // DELETE - EXLUIR moduloS POR ARRAY DE ID'S
  router.delete('/arr', auth, GenericController.removeArray.bind(GenericController));

  // DELETE - EXLUIR moduloS POR UM ID
  router.delete('/:_id', auth, GenericController.remove.bind(GenericController));

  return {
    model: GenericModel,
    controller: GenericController,
    router: router
  };
};


module.exports = function (collection, objSchema, populate) {
  return new routerCreater(collection, objSchema, populate)
};