// mongoose.js

var mongoose = require('mongoose');
var fs = require('fs');
var path = require('path');
var config = require('config');

'use strict';

function _connection() {
  var remote = config.get('db_info');
  var username = remote.username,
    password = remote.password,
    server = remote.server,
    port = remote.port,
    database = remote.database,
    auth = username ? username + ':' + password + '@' : '';
  return 'mongodb://' + auth + server + ':' + port + '/' + database;
}

mongoose.connect(_connection(), {
  useMongoClient: true
});

var db = mongoose.connection;
db.on('error', function (err) {
  console.log(err);
});

db.on('open', function (callback) {
  console.log(`connected to MongoDb`)
});

module.exports = mongoose;